<?php
add_filter( 'rwmb_meta_boxes', 'cashcarry_meta_boxes' );
function cashcarry_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => 'Gallery',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-cashcary.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            
            array(
                'id'               => 'cc_gallery',
                'name'             => 'Image Advanced',
                'type'             => 'image_advanced',
                'force_delete'     => false,
                'image_size'       => 'thumbnail',
            ),
        ),

    );
    $meta_boxes[] = array(
        'title'      => 'Text',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-cashcary.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            
            array(
                'id'   => 'cc_title',
                'name' => 'Title',
                'type' => 'text',
            ),   
            array(
                'id'   => 'cc_text',
                'name' => 'Text',
                'type' => 'textarea',
            ),  
        ),

    );
    return $meta_boxes;
}