<?php
$heading = rwmb_meta('post_heading'); 
$text = rwmb_meta('post_text'); 
$button = rwmb_meta('post_button'); 
$url = rwmb_meta('post_url'); 
$query = new WP_Query( array(
    'post_type' => 'post',
    'numberposts' => 3,
    'post_status' => 'publish',
    'meta_query' => array(
        array(
            'key'     => 'feat_post',
            'value'   => '1',
            'compare' => 'LIKE',
        ),
    ),
) );
?>
<div class="container section" about>
	<div class="home-heading-content">
		<div class="home-heading-content__heading animate animate__fade">
			<h2 class="animate animate__fade-up">{{ $heading }}</h2>
			
		</div>
		<div class="home-heading-content__button animate animate__fade mob-hidden">
			<a class="button--read-more animate animate__fade-up" href="{{ $url }}">{{ $button }}</a>
		</div>
	</div>
	<p class="about__text animate animate__fade-up">{{ $text }}</p>
	<div class="posts">

		@while($query->have_posts()) @php $query->the_post() @endphp
			@include('partials.content-post')
		@endwhile

	</div>
	<div class="button-container mob-only">
		<a href="{{ $url }}" class="button">{{ $button }}</a> 
	</div>
</div>