<?php 
	$heading = rwmb_meta('about_heading');
	$text = rwmb_meta('about_text');
	$button_text = rwmb_meta('about_button');
	$button_url = rwmb_meta('about_url');
	$images = rwmb_meta( 'cash_image', array( 'size' => 'large' ) );
	$image = reset($images);
?>
<div class="container section section--text-image content-left">
	<div class="row">

		
		<div class="col-sm-6">
			<div class="section__content ">
				<h2  class="animate animate__fade-up"><?php echo $heading; ?></h2>
				<p class="section__text animate animate__fade-up"><?php echo $text ?></p>
				<a class="button animate animate__fade-up" href="<?php echo $button_url; ?>"><?php echo $button_text; ?></a>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="section__image animate animate__fade">
				<div class="image-border"></div>
				<img alt="" src="<?php echo $image['url']; ?>" />
			</div>
		</div>


	</div>
</div>