<?php 
$lang = pll_current_language('slug'); 
$logos = rwmb_meta( 'logo', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];


$address = rwmb_meta('cc_address', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$email = rwmb_meta('cc_email', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$phone = rwmb_meta('cc_phone', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$fax = rwmb_meta('cc_fax', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');

$hours = rwmb_meta('working_hours', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');

?>
<footer>
	<div class="container">
		<div class="footer-content clearfix animate animate__fade">

			<div class="footer-content__column">
				<a class="logo animate animate__fade" href="/"><img src="{{ $header_image }}" alt="BalticXL"></a>
				<p class="copyright animate animate__fade-up">© <?php echo date("Y"); ?> <?php echo pll__('SIA "BalticXL" Visas tiesības aizsargātas.', 'General') ?></p> 
			</div>

			<div class="footer-content__column animate animate__fade-up">
				<p class="footer-content__title"><?php echo pll__('Cash & carry veikals', 'General') ?></p>
				<div class="cc-info">
                    <div class="row">
                      <div class="col-sm-6">
                          <div class="cc-info__contacts">
                            <p class="cc-info__contacts__title"><?php echo pll_e('Adrese', 'Cash and carry') ?></p>
                            <p class="cc-info__contacts__info"><?php echo $address; ?></p>
                          </div>
                          <div class="cc-info__contacts">
                            <p class="cc-info__contacts__title"><?php echo pll_e('E-pasts', 'Cash and carry') ?></p>
                            <a class="cc-info__contacts__info" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                          </div>
                          <div class="cc-info__contacts">
                            <p class="cc-info__contacts__title"><?php echo pll_e('Tālrunis', 'Cash and carry') ?></p>
                            <a class="cc-info__contacts__info" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
                          </div>
                          <div class="cc-info__contacts">
                            <p class="cc-info__contacts__title"><?php echo pll_e('Faks', 'Cash and carry') ?></p>
                            <p class="cc-info__contacts__info"><?php echo $fax; ?></p>
                          </div>

                      </div>
                      <div class="col-sm-6">
                      	<div class="whour-content">
                      		 <p class="cc-info__whour-title"><?php echo pll_e('Darbalaiks', 'Cash and carry') ?></p>
	                        <div class="cc-info__whour-info">
	                            <p class="cc-info__whour-info__title"><?php echo pll_e('Pirmdiena - Ceturdiena', 'Cash and carry') ?></p>
	                            <p class="cc-info__whour-info__info"><?php echo $hours['mon-thur']; ?></p>
	                        </div>
	                        <div class="cc-info__whour-info">
	                            <p class="cc-info__whour-info__title"><?php echo pll_e('Piektdiena', 'Cash and carry') ?></p>
	                            <p class="cc-info__whour-info__info"><?php echo $hours['friday']; ?></p>
	                        </div>
	                        <div class="cc-info__whour-info">
	                            <p class="cc-info__whour-info__title"><?php echo pll_e('Sestdiena', 'Cash and carry') ?></p>
	                            <p class="cc-info__whour-info__info"><?php echo $hours['saturday']; ?></p>
	                        </div>
	                        <div class="cc-info__whour-info">
	                            <p class="cc-info__whour-info__title"><?php echo pll_e('Svētdiena', 'Cash and carry') ?></p>
	                            <p class="cc-info__whour-info__info"><?php echo $hours['sunday']; ?></p>
	                        </div>
                      	</div>
                       
                      </div>
                    </div>
                </div><!-- END cc-info -->
			</div>

			<div class="footer-content__column animate animate__fade-up">
				<p class="footer-content__title"><?php echo pll_e('Seko mums', 'General') ?></p>
				<?php
          $platforms = array('twitter','youtube', 'facebook', 'linkedin', 'instagram', 'draugiem', 'google');
          ?>
          <ul class="socials">
              <?php foreach ($platforms as $pl) : $_pl = rwmb_meta($pl, array( 'object_type' => 'setting',  'limit' => 1), 'settings');?>
                  <?php if(!empty($_pl)) : ?>
                      <li class="social">
                          <a class="soc-item" target="_blank" href="<?php echo esc_url($_pl); ?>">
                          	<span class="social__icon"><?php echo file_get_contents(get_template_directory().'/assets/images/'.$pl.'.svg'); ?></span>
                              <span class="social__text"><?php echo $pl; ?></span>                          
                          </a>
                      </li>
                      <?php
                  endif;
              endforeach; ?>
          </ul>
       <p class="footer-content__title"><?php echo pll_e('Informācija', 'General') ?></p>
       <div class="footer-menu">
        @if (has_nav_menu('footer_navigation'))
          {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav']) !!}
        @endif
        </div>
			</div>

		</div><!-- END footer-content -->
	</div>
</footer>

