{{--
  Page for single job
--}}
@extends('layouts.app')
@section('content')
@include('layouts.page-header')

<div class="container">
	<div class="row">
		<div class="col-sm-2 d-none d-lg-block">
			<div class="share">
		      <p><?php echo pll__('Share this article', 'General'); ?></p>
		      <a class="share__icon animate animate__fade-up"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/faceboook.svg"); ?></a>
		    </div>
		</div>
		<div class="col-sm-7 col-sm-push-7">
			<div class="entry-content animate animate__fade-up">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
		<div class="col-sm-3 col-sm-push-3">
			<div class="jobs-info animate animate__fade-up">
				<?php if ( rwmb_meta( 'pub_date' ) != "" ) : ?>
					<h4 class="jobs-info__title"><span class="jobs-info__icon"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/humana-icon-publication_date.svg"); ?></span><?php echo pll__('Publication date:', 'Job-page'); ?></h4>
					<p class="jobs-info__text"><?php echo rwmb_meta('pub_date'); ?></p>
				<?php endif; ?>
				<?php if ( rwmb_meta( 'job_title' ) != "" ) : ?>
					<h4 class="jobs-info__title"><span class="jobs-info__icon"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/humana-icon-job_title.svg"); ?></span><?php echo pll__('Job title:', 'Job-page'); ?></h4>
					<p class="jobs-info__text"><?php echo rwmb_meta('job_title'); ?></p>
				<?php endif; ?>
				<?php if ( rwmb_meta( 'work_load' ) != "" ) : ?>
					<h4 class="jobs-info__title"><span class="jobs-info__icon"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/humana-icon-workload.svg"); ?></span><?php echo pll__('Work load:', 'Job-page'); ?></h4>
					<p class="jobs-info__text"><?php echo rwmb_meta('work_load'); ?></p>
				<?php endif; ?>
				<?php if ( rwmb_meta( 'experiance' ) != "" ) : ?>
					<h4 class="jobs-info__title"><span class="jobs-info__icon"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/humana-icon-experience.svg"); ?></span><?php echo pll__('Experience:', 'Job-page'); ?></h4>
					<p class="jobs-info__text"><?php echo rwmb_meta('experiance'); ?></p>
				<?php endif; ?>
				<?php if ( rwmb_meta( 'work_time' ) != "" ) : ?>
					<h4 class="jobs-info__title"><span class="jobs-info__icon"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/humana-icon-type_of_working_time.svg"); ?></span><?php echo pll__('Type of working time:', 'Job-page'); ?></h4>
					<p class="jobs-info__text"><?php echo rwmb_meta('work_time'); ?></p>
				<?php endif; ?>
			</div>
		</div>

	</div>
	
</div>

@endsection