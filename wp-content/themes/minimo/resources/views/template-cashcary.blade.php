{{--
  Template Name: Cash & Carry
--}}
@extends('layouts.app')
@section('content')
  <?php 
    $address = rwmb_meta('cc_address', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
    $email = rwmb_meta('cc_email', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
    $phone = rwmb_meta('cc_phone', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
    $fax = rwmb_meta('cc_fax', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');

    $hours = rwmb_meta('working_hours', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  ?>
  @while(have_posts()) @php the_post() @endphp
    <div class="content-cashcarry">
      <div class="full-width-block">
          

          <div class="container">
            <div class="row">

              <div class="col-sm-6">
                <h1 class="page-title"><?php echo get_the_title(); ?></h1>
                <div class="cc-info animate animate__fade-up">
                    <div class="row">

                      <div class="col-sm-6">
                          <div class="cc-info__contacts">
                            <p class="cc-info__contacts__title"><?php echo pll_e('Adrese', 'Cash and carry') ?></p>
                            <p class="cc-info__contacts__info"><?php echo $address; ?></p>
                          </div>
                          <div class="cc-info__contacts">
                            <p class="cc-info__contacts__title"><?php echo pll_e('E-pasts', 'Cash and carry') ?></p>
                            <a class="cc-info__contacts__info" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                          </div>
                          <div class="cc-info__contacts">
                            <p class="cc-info__contacts__title"><?php echo pll_e('Tālrunis', 'Cash and carry') ?></p>
                            <a class="cc-info__contacts__info" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
                          </div>
                          <div class="cc-info__contacts">
                            <p class="cc-info__contacts__title"><?php echo pll_e('Faks', 'Cash and carry') ?></p>
                            <p class="cc-info__contacts__info"><?php echo $fax; ?></p>
                          </div>

                      </div>

                      <div class="col-sm-6">
                        <div class="whour-content">
                          <p class="cc-info__whour-title"><?php echo pll_e('Darbalaiks', 'Cash and carry') ?></p>
                          <div class="cc-info__whour-info">
                              <p class="cc-info__whour-info__title"><?php echo pll_e('Pirmdiena - Ceturdiena', 'Cash and carry') ?></p>
                              <p class="cc-info__whour-info__info"><?php echo $hours['mon-thur']; ?></p>
                          </div>
                          <div class="cc-info__whour-info">
                              <p class="cc-info__whour-info__title"><?php echo pll_e('Piektdiena', 'Cash and carry') ?></p>
                              <p class="cc-info__whour-info__info"><?php echo $hours['friday']; ?></p>
                          </div>
                          <div class="cc-info__whour-info">
                              <p class="cc-info__whour-info__title"><?php echo pll_e('Sestdiena', 'Cash and carry') ?></p>
                              <p class="cc-info__whour-info__info"><?php echo $hours['saturday']; ?></p>
                          </div>
                          <div class="cc-info__whour-info">
                              <p class="cc-info__whour-info__title"><?php echo pll_e('Svētdiena', 'Cash and carry') ?></p>
                              <p class="cc-info__whour-info__info"><?php echo $hours['sunday']; ?></p>
                          </div>
                        </div>
                      </div>

                    </div>
                </div><!-- END cc-info -->
              </div>

              <div class="col-sm-6">
                
              </div>

            </div>
          </div><!-- END container -->

          <div class="full-width-block__left">
            
          </div>
          <div class="full-width-block__right">
             <?php
              $map = rwmb_get_value( 'cc_map', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
              $pins = rwmb_meta('cc_map_pin', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
              $pin = reset( $pins );

              ?>
              <?php if (!empty($map)) : ?>
                  <div class="content-cashcarry__map animate animate__fade">
                    <div id="map-wrap"
                           class="content-cashcarry__map__wrap"
                           data-lng="<?php echo $map['longitude'] ?>"
                           data-lat="<?php echo $map['latitude'] ?>"
                           data-zoom="<?php echo $map['zoom']; ?>"
                           data-pin="<?php echo $pin['url']; ?>">
                             
                    </div>
                  </div>
              <?php endif; ?>
          </div><!-- END full-width-block__right -->

      </div><!-- END full-width-block -->

      <div class="section container">
        <div class="row">

          <div class="col-sm-6">

            <div class="gallery-block animate animate__fade">

              <div class="gallery">
                <?php 
                $images = rwmb_meta( 'cc_gallery', array( 'size' => 'large' ) ); 
                $count = 0;
                ?>

                  <?php foreach ( $images as $image ) : ?>

                      <div class="gallery__item" data-item="<?php echo $count; ?>">
                        <div class="gallery__item__image">
                           <img data-src='<?php echo $image['full_url']; ?>' data-width="<?php echo $image['sizes']['large']['width']; ?>" data-height="<?php echo $image['sizes']['large']['height']; ?>" src="<?php echo $image['url']; ?>" />
                        </div>
                       
                      </div>
                      <?php $count++; ?>
                  <?php endforeach; ?>

              </div>

              <div class="gallery-controls clearfix">
                <div class="left">
                  <a class="gallery-controls__open">Skatīt galeriju</a>
                </div>
                <div class="right">
                   <span class="gallery-controls__arrow-left"><?php echo file_get_contents(get_template_directory().'/assets/images/arrow-left.svg'); ?></span>
                  <span class="gallery-controls__arrow-right"><?php echo file_get_contents(get_template_directory().'/assets/images/arrow-right.svg'); ?></span>
                  <span class="gallery-controls__paging"></span>
                </div>
              </div>

              </div><!-- END galler-block -->

            </div>

          <div class="col-sm-6">
            <?php 
              $cc_title = rwmb_meta( 'cc_title' ); 
              $cc_text  = rwmb_meta( 'cc_text' ); 
            ?>
            <div class="cc-text">
              <h2 class="animate animate__fade-up"><?php echo $cc_title; ?></h2>
              <p class="animate animate__fade-up"><?php echo $cc_text; ?></p>
            </div>
          </div>
        </div>
      </div><!-- END section -->

    </div>
  @endwhile
@endsection


   			