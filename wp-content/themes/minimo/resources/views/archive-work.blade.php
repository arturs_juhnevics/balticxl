@extends('layouts.app')

@section('content')
@include('layouts.page-header')
<div class="container work">
	<div class="row">
  		@while(have_posts()) @php the_post() @endphp
  			<?php 
  			$feat_image = get_the_post_thumbnail_url();
  			?>
  			<div class="col-sm-4">
  				<a href="<?php echo get_permalink( ); ?>">
	  				<div class="work__item animate animate__fade" style="background-image: url({{ $feat_image }});">
	  					<div class="overlay">
	  					</div>
	  					<a class="button">View more</a>
	  				</div>
	  				<div class="work__info animate animate__fade">
	  					<h3><?php echo get_the_title(); ?></h3>
	  				</div>
  				</a>
  			</div>
	   @endwhile
	</div>
</div>
@endsection