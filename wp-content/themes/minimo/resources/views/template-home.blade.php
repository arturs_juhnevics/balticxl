{{--
  Template Name: Home
--}}
@extends('layouts.app')
@section('content')

<?php 
$enable_posts = rwmb_meta('enable_posts'); 
$estore = rwmb_meta( 'enable_estore', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
?>

  @while(have_posts()) @php the_post() @endphp
    @include('partials.home.hero')
    @include('partials.home.cashcarry')
    <?php if ( $estore ) : ?>
      @include('partials.home.estore')
    <?php endif; ?>
    @include('partials.home.about')
    <?php if( $enable_posts == true) : ?>
    	@include('partials.home.posts')
    <?php endif; ?>
  @endwhile
@endsection