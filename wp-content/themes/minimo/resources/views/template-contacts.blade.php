{{--
  Template Name: Contacts
--}}
@extends('layouts.app')
@section('content')
@include('layouts.page-header-simple')
<?php 
  $address = rwmb_meta('cc_address', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $email = rwmb_meta('cc_email', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $phone = rwmb_meta('cc_phone', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $fax = rwmb_meta('cc_fax', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');

  $rek_address = rwmb_meta('rek_address');  
  $rek_numurs = rwmb_meta('rek_numurs');
  $rek_pvn = rwmb_meta('rek_pvn');
  $rek_bank = rwmb_meta('rek_bank');
?>
  @while(have_posts()) @php the_post() @endphp

    <div class="contacts container">

      <div class="row">

        <div class="col-sm-3 contacts-column animate animate__fade-up">
          <div class="cc-info">
            <div class="cc-info__contacts">
              <p class="cc-info__contacts__title"><?php echo pll_e('Biroja adrese', 'Contacts') ?></p>
              <p class="cc-info__contacts__info"><?php echo $address; ?></p>
            </div>
            <div class="cc-info__contacts">
              <p class="cc-info__contacts__title"><?php echo pll_e('E-pasts', 'Cash and carry') ?></p>
              <a class="cc-info__contacts__info" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
            </div>
            <div class="cc-info__contacts">
              <p class="cc-info__contacts__title"><?php echo pll_e('Tālrunis', 'Cash and carry') ?></p>
              <a class="cc-info__contacts__info" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
            </div>
            <div class="cc-info__contacts">
              <p class="cc-info__contacts__title"><?php echo pll_e('Faks', 'Cash and carry') ?></p>
              <p class="cc-info__contacts__info"><?php echo $fax; ?></p>
            </div>
          </div>
        </div>

        <div class="col-sm-3 contacts-column animate animate__fade-up">
          <div class="cc-info">
            <div class="cc-info__contacts">
              <p class="cc-info__contacts__title"><?php echo pll_e('Juridiskā adrese', 'Contacts') ?></p>
              <p class="cc-info__contacts__info"><?php echo $rek_address; ?></p>
            </div>
            <div class="cc-info__contacts">
              <p class="cc-info__contacts__title"><?php echo pll_e('Reģistrācijas numurs', 'Contacts') ?></p>
              <p class="cc-info__contacts__info" ><?php echo $rek_numurs; ?></p>
            </div>
            <div class="cc-info__contacts">
              <p class="cc-info__contacts__title"><?php echo pll_e('PVN reģistrācijas numurs', 'Contacts') ?></p>
              <p class="cc-info__contacts__info"><?php echo $rek_pvn; ?></p>
            </div>
            <div class="cc-info__contacts">
              <p class="cc-info__contacts__title"><?php echo pll_e('Bankas konts', 'Contacts') ?></p>
              <p class="cc-info__contacts__info"><?php echo $rek_bank; ?></p>
            </div>
          </div>
        </div>

        <div class="col-sm-6 contacts-column animate animate__fade-up">
          
          <div class="contacts__form">
            <form id="contact-form" method="POST">
                <div class="row">
                    <div class="col-sm-6 animate animate__fade-up">
                        <label class="input__block">
                            <span class="input__block__label"><?php echo pll__('Vārds', 'Contact-form'); ?><span class="req">*</span></span>
                            <input type="text" name="name" id="name" class="required"/>
                        </label>
                    </div>
                    <div class="col-sm-6 animate animate__fade-up">
                       <label class="input__block">
                            <span class="input__block__label"><?php echo pll__('Telefons', 'Contact-form'); ?><span class="req">*</span></span>
                            <input type="text" name="phone" id="phone" class="required"/>
                        </label>
                    </div>
                    <div class="col-sm-12 animate animate__fade-up">
                        <label class="input__block">
                            <span class="input__block__label"><?php echo pll__('E-pasts', 'Contact-form'); ?><span class="req">*</span></span>
                            <input type="text" name="email" id="email" class="required"/>
                        </label>
                    </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 animate animate__fade-up">
                       <label class="input__block">
                            <span class="input__block__label"><?php echo pll__('Ziņa', 'Contact-form'); ?><span class="req">*</span></span>
                            <textarea name="message" id="message" > </textarea>
                        </label>
                    </div>
                </div>
                    <div id="recaptcha_element"></div>
                    <div class="button-container">
                        <div class="form__status hidden"></div>
                        <button class="button" class="contacts__form__submit"><?php echo pll__('Sūtīt', 'Contact-form'); ?></button>
                    </div>
                    <div class="col-sm-12">
                        <?php wp_nonce_field('contact-nonce', 'contact-nonce'); ?>
                        <?php $from = '';
                        if (isset($_GET['from'])) {
                            $from = strip_tags($_GET['from']);
                        } ?>
                        <input type="hidden" name="page_from" id="page_from" value="<?php echo $from; ?>" class="leave"/>
                        <input type="hidden" name="post_id" id="post_id" value="<?php echo $post->ID; ?>" class="leave"/>
                        <input type="hidden" name="action" id="action" value="contact_submit" class="leave"/>
                        <input type="text" name="contact-honey" id="contact-honey" class="hidden"/>
                        
                    </div>
                  </form>
              </div>

        </div>   

      </div>

    </div>

  @endwhile
  
@endsection


   			