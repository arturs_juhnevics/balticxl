var home = {
	init: function (){

		$('.hero').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          fade: true,
          autoplay: true,
          arrows: false,
          autoplaySpeed: 4000,
          adaptiveHeight: false,
          draggable: true,
          pauseOnHover: false,
          pauseOnFocus: false,
          dots:true,
          lazyLoad: 'ondemand',
          appendDots: $('.slider-dots'),

       });

		var count = 1;
		$('.slick-dots li').each( function() {
			$(this).prepend('<span>0'+ count +'</span>');
			count++;
		});

     $('.posts').not('.slick-initialized').slick({
         slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          fade: false,
          autoplay: true,
          arrows: false,
          autoplaySpeed: 5000,
          adaptiveHeight: false,
          draggable: true,
  
          prevArrow: $('.arrow-left'),
          nextArrow: $('.arrow-right'),
          responsive: [
              {
                  breakpoint: 890,
                  settings: {
                    slidesToShow: 2,
                    centerMode: true,
                    centerPadding: '30px',
                  }
              },
              {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '30px',
                  }
              }
          ]
       });
	}
}
home.init();