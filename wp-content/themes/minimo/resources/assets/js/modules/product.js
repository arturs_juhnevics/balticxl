var product = {
    init: function () {
      var $slickElement = $('.gallery__main__inner');
      var $thumbs = $('.gallery__thumbs__inner');
      var items = $('.gallery__main__item').length;

       $slickElement.slick({
             slidesToShow: 1,
            slidesToScroll: 1,
            lazyload: "ondemand",
            fade: false,
            autoplay: false,
            arrows: false,
            adaptiveHeight: false,
       });

        // $thumbs.slick({
        //     slidesToShow: 5,
        //     slidesToScroll: 1,
        //     asNavFor: $slickElement,
        //     dots: true,
        //     arrows: false,
        //     focusOnSelect: true,
        //     responsive: [
        //         {
        //             breakpoint: 992,
        //             settings: {
        //                 slidesToShow: 5,
        //                 infinite: items > 5,
        //             },
        //         },
        //         {
        //             breakpoint: 500,
        //             settings: {
        //                 slidesToShow: 3,
        //                 infinite: items > 3,
        //             },
        //         },
        //     ],
        // });

        $('.gallery__thumbs__item').click(function() {
                   $slickElement.slick('slickGoTo',$(this).index());
        })

        if($('.gallery__main__inner').hasClass("slick-initialized")){
            $(".gallery__thumbs").removeClass("n-visiable");
        }
    },
    
}
product.init();