<div id="lookbook-gallery" class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    
    <div class="pswp__bg"></div>

    <div class="pswp__scroll-wrap">

        <div class="pswp__container">
    		<div class="pswp__item"></div>
    		<div class="pswp__item"></div>
    		<div class="pswp__item"></div>
        </div>

        <div class="pswp-hotspots"></div>
        <div class="desc-wrap">
            <div class="close-wrap">
                <a href="#" class="desc-close">
                    <i class="fa fa-chevron-right"></i>
                </a>
            </div>
            <div class="pin-desc" data-id="0">
                <div class="entry-content">
                    
                </div>
            </div>
        </div>

        <div class="gal-info"><span class="current">1</span> / <span class="total">8</span></div>

        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <div class="pswp-zoom-slider">
                    <div id="range"></div>
                </div>

    			

                <div class="pswp-gallery-title"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"><svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="citi-dizaini-labots-20.11.2018-klienta-komentari-iteracja-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="Mobile-burger-open-Copy" transform="translate(-325.000000, -33.000000)" fill="#FFFFFF" fill-rule="nonzero"> <g id="solutions-nav-mobile"> <g id="menu" transform="translate(303.000000, 12.000000)"> <path d="M28.8823529,30 L22,23.1176471 L24.1176471,21 L31,27.8823529 L37.8823529,21 L40,23.1176471 L33.1176471,30 L40,36.8823529 L37.8823529,39 L31,32.1176471 L24.1176471,39 L22,36.8823529 L28.8823529,30 Z" id="burger-close"></path> </g> </g> </g> </g></svg></button>

                <a href="#" class="image-download-btn" style="display:none" target="_blank"><i class="fa fa-cloud-download"></i><?php echo __('Download'); ?></a>

                <?php get_template_part( 'templates/gallery/share' ); ?>
                
    			<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
    			<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

    			<div class="pswp__preloader">
    				<div class="pswp__preloader__icn">
    				    <div class="pswp__preloader__cut">
    				        <div class="pswp__preloader__donut"></div>
    				    </div>
    				</div>
    			</div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"><svg width="20px" height="16px" viewBox="0 0 20 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="citi-dizaini-labots-20.11.2018-klienta-komentari-iteracja-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="solutions-home-1-b" transform="translate(-1252.000000, -2390.000000)" fill="#6B766E" fill-rule="nonzero"> <g id="realizetie-projekti" transform="translate(0.000000, 1834.000000)"> <g id="slider" transform="translate(1231.000000, 474.000000)"> <g id="prew" transform="translate(0.000000, 60.000000)"> <path d="M21.9999993,31 L37.5799993,31 L32.2899993,36.29 C31.9000215,36.6830079 31.9000215,37.3169921 32.2899993,37.71 C32.6800368,38.0977236 33.3099619,38.0977236 33.6999993,37.71 L40.6999993,30.71 C40.8906404,30.5235699 40.9989303,30.2687228 41.0008081,30.0020832 C41.0026858,29.7354436 40.8979959,29.4790965 40.7099993,29.29 L33.6999993,22.29 C33.5137387,22.1052558 33.2623392,22.0011046 32.9999993,22 C32.7347016,22.0010428 32.4801633,22.1050091 32.2899993,22.29 C31.9000215,22.6830079 31.9000215,23.3169921 32.2899993,23.71 L37.5799993,29 L21.9999993,29 C21.4477148,29.0000004 21,29.4477155 21,30 C21,30.5522845 21.4477148,30.9999996 21.9999993,31 Z" id="arrow_left" transform="translate(31.000416, 30.000396) rotate(-180.000000) translate(-31.000416, -30.000396) "></path> </g> </g> </g> </g> </g></svg></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"><svg width="20px" height="16px" viewBox="0 0 20 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="citi-dizaini-labots-20.11.2018-klienta-komentari-iteracja-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="solutions-home-1-b" transform="translate(-1251.000000, -2329.000000)" fill="#6B766E" fill-rule="nonzero"> <g id="realizetie-projekti" transform="translate(0.000000, 1834.000000)"> <g id="slider" transform="translate(1231.000000, 474.000000)"> <g id="next"> <path d="M20.9999993,30 L36.5799993,30 L31.2899993,35.29 C30.9000215,35.6830079 30.9000215,36.3169921 31.2899993,36.71 C31.6800368,37.0977236 32.3099619,37.0977236 32.6999993,36.71 L39.6999993,29.71 C39.8906404,29.5235699 39.9989303,29.2687228 40.0008081,29.0020832 C40.0026858,28.7354436 39.8979959,28.4790965 39.7099993,28.29 L32.6999993,21.29 C32.5137387,21.1052558 32.2623392,21.0011046 31.9999993,21 C31.7347016,21.0010428 31.4801633,21.1050091 31.2899993,21.29 C30.9000215,21.6830079 30.9000215,22.3169921 31.2899993,22.71 L36.5799993,28 L20.9999993,28 C20.4477148,28.0000004 20,28.4477155 20,29 C20,29.5522845 20.4477148,29.9999996 20.9999993,30 Z" id="arrow_right" transform="translate(30.000416, 29.000396) scale(-1, 1) rotate(-180.000000) translate(-30.000416, -29.000396) "></path> </g> </g> </g> </g> </g></svg></button>

            <div class="pswp__caption">
             	<div class="pswp__caption__center">
            	</div>
            </div>
      </div>
    </div>
</div>