<div <?php post_class() ?>>
<div class="container">
  <div class="entry-wrapper">
    <div class="entry-content">
      <div class="post-title">
        <p class="post-date"><?php echo get_the_date("d, F, Y"); ?></p>
        <h1><?php echo get_the_title(); ?></h1>
      </div>
      <?php the_content() ?>
    </div>
  </div>
    <div class="related-posts">
      <div class="related-posts__nav">
        <h2><?php echo pll__('More stories', 'Post') ?></h2>
        <div class="related-posts__nav__controls slick-controls mob-hidden">
          <span class="arrow-left"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/chevron-left.svg"); ?></span>
          <span class="arrow-right"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/chevron-right.svg"); ?></span>
        </div>
      </div>
      <div class="post-slider">
        <?php 
          $query = new WP_Query( 
            array( 
              'post_type' => 'post',
              'posts_per_page'=> 6, 
              'post__not_in' => array(get_the_ID()),
            ) 
          );
          ?>
          <?php while ($query->have_posts()) : $query->the_post(); ?> 
            <?php echo $__env->make('partials.content-post', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php endwhile; ?>
      </div>
    </div>

  </div>

</div>
