<?php $__env->startSection('content'); ?>

<?php 
$enable_posts = rwmb_meta('enable_posts'); 
$estore = rwmb_meta( 'enable_estore', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
?>

  <?php while(have_posts()): ?> <?php the_post() ?>
    <?php echo $__env->make('partials.home.hero', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('partials.home.cashcarry', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php if ( $estore ) : ?>
      <?php echo $__env->make('partials.home.estore', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
    <?php echo $__env->make('partials.home.about', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php if( $enable_posts == true) : ?>
    	<?php echo $__env->make('partials.home.posts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
  <?php endwhile; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>