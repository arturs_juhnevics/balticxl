<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container work">
	<div class="row">
  		<?php while(have_posts()): ?> <?php the_post() ?>
  			<?php 
  			$feat_image = get_the_post_thumbnail_url();
  			?>
  			<div class="col-sm-4">
  				<a href="<?php echo get_permalink( ); ?>">
	  				<div class="work__item animate animate__fade-up" style="background-image: url(<?php echo e($feat_image); ?>);">
	  					<div class="overlay">
	  					</div>
	  					<a class="button">View more</a>
	  				</div>
	  				<div class="work__info animate animate__fade-up">
	  					<h3><?php echo get_the_title(); ?></h3>
	  				</div>
  				</a>
  			</div>
	   <?php endwhile; ?>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>