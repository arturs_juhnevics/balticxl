<div class="agecheck">
	<div class="overlay overlay--solid"></div>
	<div class="agecheck__content">
		<h2><?php echo pll_e('Uzmanību!', 'Age check'); ?></h2>
		<p><?php echo pll_e('Vai jums jau ir 18 gadu?', 'Age check'); ?></p>
		<div class="btn-container">
			<div class="agecheck__button">
				<a href="" id="agecheck-true" class="button"><?php echo pll_e('Jā', 'Age check'); ?></a> 
			</div>
			<div class="agecheck__button">
				<a href="" id="agecheck-false" class="button"><?php echo pll_e('Nē', 'Age check'); ?></a> 
			</div>
		</div>
	</div>
</div>