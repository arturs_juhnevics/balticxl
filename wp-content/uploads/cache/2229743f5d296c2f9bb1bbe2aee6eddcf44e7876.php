<div class="blocks contact">
	<div class="blocks__item">
		<div class="blocks__item__left"></div>
		<div class="blocks__item__right"></div>
		<div class="container">
			<div class="block__content row" id="contact">
				<div class="block__content__left col-sm-6">
					<div class="block-inner">
						<h2 class="git-heading animate animate__fade-up">ORDER YOUR CUSTOM FURNITURE</h2>
						<?php echo $__env->make('partials.home.getintouch', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					</div>
				</div>
				<div class="block__content__right col-sm-6">
					<div class="block-inner" id="order">
						<p class="git-info animate animate__fade-up">EMAIL <span class="git-info__content"><a href="mailto:info@southerncharmfinedesign.com">info@southerncharmfinedesign.com</a></span></p>
						<p class="git-info animate animate__fade-up">PHONE <span class="git-info__content"><a href="tel:7614563113">761-456-3113</a></span></p>
						<div class="git-social animate animate__fade-up"><a href="https://www.facebook.com/southerncharmfinedesign/" target="_blank"><span class="facebook git-social__item"><i class="fab fa-facebook-square"></i>FACEBOOK</span></a> <a href="" target="_blank"><span class="instagram git-social__item"><i class="fab fa-instagram"></i>INSTAGRAM</span></a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>