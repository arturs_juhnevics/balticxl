<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.page-header-simple', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
$description = rwmb_meta( 'p_archive_text', array( 'object_type' => 'setting'), 'settings');
?>
<div class="container">
	<div class="archive-desc animate animate__fade">
		<p><?php echo e($description); ?></p>
	</div>
</div>

<div class="container products">
	<div class="row">
  		<?php while(have_posts()): ?> <?php the_post() ?>
  			<?php 
  			$image = get_the_post_thumbnail_url();
			$title = get_the_title(); 
			$url = get_the_permalink();
  			?>
  			<div class="col-sm-4">
  				<a href="<?php echo e($url); ?>">
					<div class="product-item--medium product-item animate animate__fade" style="background-image: url(<?php echo e($image); ?>)">
						<div class="overlay"></div>
						<div class="button-overlay"><p class="button--read-more">VIEW PRODUCT</p></div>
						<p class="product-item__title"><?php echo e($title); ?></p>
					</div>
				</a>
  			</div>
	   <?php endwhile; ?>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>