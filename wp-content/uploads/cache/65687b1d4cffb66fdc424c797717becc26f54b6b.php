<?php 
$id = get_the_ID();
$short_text = rwmb_meta('post_short', false, $id); 
$image = get_the_post_thumbnail_url($id, 'medium');
$theme = ($image != false) ? "dark" : "light"; 
$url = get_the_permalink();
?>

<a href="<?php echo e($url); ?>">
<div class="posts__item animate animate__fade" style="background-image: url(<?php echo e($image); ?>)">
	<?php if( $image ) : ?>
		<div class="overlay"></div>
	<?php endif; ?>
	<div class="posts__item__content  <?php echo e($theme); ?>">
		<p class="posts__item__content__date"><?php echo get_the_date("d, F, Y"); ?></p>
		<h3 class="posts__item__content__title"><?php echo get_the_title(); ?></h3>
		<p class="posts__item__content__text"><?php echo e($short_text); ?></p>
	</div>
</div>
</a>
