<?php
$heading = rwmb_meta('partners_heading'); 
$heading_slug = rwmb_meta('partners_slug'); 
$text = rwmb_meta('partners_text'); 
$icons = rwmb_meta('partners'); 
$empty_text = ($text == "") ? true : false; 
?>
<div class="container home-section">
	<div class="home-heading-content">
		<div class="home-heading-content__heading">
			<p class="home-heading-content__slug"><?php echo e($heading_slug); ?></p>
			<h2 class="home-heading-content__title"><?php echo e($heading); ?></h2>
		</div>
	</div>
	<div class="content partners <?php if($empty_text){echo " no-text";} ?>">
		<div class="row">
 			<?php if(!$empty_text) : ?>
				<div class="col-sm-6">
					<p class="partners__text animate animate__fade"><?php echo e($text); ?></p>
				</div>
			<?php endif; ?>
			<div class="<?php if( $empty_text ){ echo "col-sm-12"; }else{ echo "col-sm-6"; } ?>">
				<div class="partners__icons animate animate__fade">
					<?php foreach ($icons as $item ) : ?>
						<?php	

						$name = (isset($item['partners_name'])) ? $item['partners_name'] : "partner"; 
						$url = (isset($item['partners_url'])) ? $item['partners_url'] : false; 
						$image_ids = $item['partners_icon'];
             			$image = RWMB_Image_Field::file_info( $image_ids[0], array( 'size' => 'medium' ));
						?>
						<?php if($url) :?><a href="<?php echo e($url); ?>" target="_blank"> <?php endif; ?> 
							<img alt="<?php echo e($name); ?>" src="<?php echo e($image['url']); ?>"/>
						<?php if($url) :?></a><?php endif; ?>
						
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>